# Abstrct
This is a scratch lrzsz-0.12.20 zmodem-receiving porting for U-Boot.
It adds a new command 'loadz' and has same syntax as loady.
Be free to trial this patch (of course, no warranty). 

In U-Boot, ZModem vs. YModem by Teraterm:
@115200bps    10.5K/s / 9.0K/s
@5Mbps        180K/s    140K/s	

Terminal application also matters. For transferring with 5Mbps USB-RS422,
ZModem in Teraterm reaches about 200KB/s while SecureCRT gets 300KB/s. 

Why only 300KB/s (2.5Mbps)? Probably cable issue or USB frame interval characteristic. 
Just a guess.

# How to apply the patches
*Copy following files to U-Boot common/ folder*

* config.h
* error.h
* lrz.c
* zglobal.h
* zm.c
* zmodem.h
* zreadline.c
* rbsb.c

*Apply changes*

* load.c.diff             ----> cmd/load.c
* Makefile.diff           ----> common/Makefile
* xyzModem.c.diff         ----> common/xyzModem.c


# Porting to other platform
With above changes, you should be able to run zmodem receiving in originally working U-Boot.
For porting to other application, ex. non-OS application. Search "PORTING"  in lrz.c and apply changes to these functions. 

* void sendline(int c)
* int read_data(int tout_in_100ms, char *buf, int size)
* void send_data(int fd, char *buf, int size)
* void flushmo()
* double timing (int reset, time_t *nowp)

